import { FlexLayoutModule } from '@angular/flex-layout';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MatSliderModule } from '@angular/material/slider';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';

//Imports de Angular Material

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './components/login/login.component';
import {MatFormFieldModule, MatLabel} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatBadgeModule} from '@angular/material/badge';
import {MatCardModule} from '@angular/material/card';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatIconModule } from '@angular/material/icon';
import { RegisterComponent } from './components/register/register.component';
import {MatSelectModule} from '@angular/material/select';
import {MatToolbarModule} from '@angular/material/toolbar'; 
import {MatGridListModule} from '@angular/material/grid-list'; 
import { MatSidenavModule } from '@angular/material/sidenav';
import { ProyectosComponent } from './pages/proyectos/proyectos.component';
import {MatMenuModule} from '@angular/material/menu';
import {MatTableDataSource, MatTableModule} from '@angular/material/table';
import {MatTabsModule} from '@angular/material/tabs';

import {MatListModule} from '@angular/material/list';
import {MatChipsModule} from '@angular/material/chips';
import {MatDividerModule} from '@angular/material/divider';
import {MatStepperModule} from '@angular/material/stepper';
import {MatDatepickerModule} from '@angular/material/datepicker'; 

import {MatTooltipModule} from '@angular/material/tooltip';


//Datatable
import {MatPaginatorModule} from '@angular/material/paginator';
import { PerfilComponent } from './pages/perfil/perfil.component';
import { AgregarproyectosComponent } from './pages/agregarproyectos/agregarproyectos.component';
import { ProyectoComponent } from './pages/proyecto/proyecto.component';
import { PrincipalComponent } from './pages/principal/principal.component';
import { MatNativeDateModule } from '@angular/material/core';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    ProyectosComponent,
    PerfilComponent,
    AgregarproyectosComponent,
    ProyectoComponent,
    PrincipalComponent,
    
  ],
  imports: [
BrowserModule,
  MatIconModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatSliderModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatBadgeModule,
    MatCardModule,
    MatSidenavModule,
    MatCheckboxModule,
    MatListModule,
    MatSlideToggleModule,
    MatIconModule,
    MatSelectModule,
    MatMenuModule,
    FormsModule,
    MatTabsModule,
    MatTableModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    MatToolbarModule, 
    MatGridListModule,
    FlexLayoutModule,
    MatPaginatorModule,
    MatChipsModule,
    MatDividerModule,
    MatTooltipModule,
    MatStepperModule,
    MatTooltipModule,
    MatPaginatorModule,
    MatDatepickerModule,
    MatNativeDateModule
  ],
  
  providers: [],
  bootstrap: [AppComponent],
  schemas:[
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class AppModule { }