import { HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import { Login} from '../models/login.interface';
import { Request} from '../models/request.interface';


@Injectable({
  providedIn: 'root'
})
export class UserServicesService {

  URL='http://localhost:3000/';
  constructor(private http: HttpClient) {    
   }

login(login: Login):Observable<Request>{
  return this.http.post<Request>(`${this.URL}innov/login`,login);
}

getProyectos(){
  return this.http.get(`${this.URL}innov/project/list`);
}

getProyecto(id: number){
  return this.http.get(`${this.URL}innov/project/${id}`);
}

}
